# iconiCSS [WIP]
A simple tool for generating swappable CSS style sheets for icon packs.

## License
This project is released under the GNU GPL-3.0 license.
Check out the [LICENSE](LICENSE) file for more information. 
